# IRIS foreach macro

ObjectScript macro that implements a `foreach` macro.

## Usage

Import the file `ForEach.inc` into the namespace where you want to use it.
Include the file in your class by putting `Include ForEach` at the top of your class file.

In your code, write

```objectscript
$$$foreach(<%array>,<%key>,<%code>)
```

Here, `%array` is a subscripted variable, `%key` is the array index (it doesn't have to be defined upfront), and `%code` is the code you want to execute for each iteration.

An example is provided in the *Foreach.Test* class.

## Background

ObjectScript does not provide a `foreach` language construct.
There is [a Community post](https://community.intersystems.com/post/foreach-objectscript) that addresses this subject.

This snippet is a slight adaptation of that example.
It is usable both in a routine and in a class context.
Because a macro is basically text substitution, all in-scope variables are available to the `%code` argument.

## Limitations

The `%code` argument cannot contain naked commas.
However, commas *are* allowed inside parentheses (i.e. between function arguments)
and inside strings. Often, naked commas can be replaced by concatenation or repetition.
As an example, the expression `write x,!` is not allowed, because the comma is interpreted by the macro preprocessor as an argument delimiter. Instead, use `write x write !` or `write x_$c(13,10)`.

The iteration only traverses the top-level index of `%array`.

Macros are bound to a namespace. You can map an include file to another namespace by creating a routine mapping for the target namespace. Note that the routine name is referenced without the `.inc` extension.
