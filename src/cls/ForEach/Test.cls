Include ForEach

Class ForEach.Test [ Abstract ]
{

/// Illustrates the $$$foreach macro with a convoluted method
/// for calculating a sum of array elements.
ClassMethod Sum() As %Status
{
	for i=1:1:200 set arr(i)=i
	set sum=0
	$$$foreach(arr,k,set sum=sum+arr(k))
	write "sum=",sum,!
	quit $$$OK
}

}
